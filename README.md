## Presentación Tails

![tails](img/tails_browser.png)

Esto es Tails: https://tails.net/

Realizada con reveal: [https://github.com/hakimel/reveal.js/](https://github.com/hakimel/reveal.js/)


## Estructura archivos

`/img/` -> carpeta de las imagenes de la presentación

`/css/theme/white.css` -> es el CSS que se esta usando en esta presentación.

`/tails_intro.md` -> es el markdown externo usado en esta presentación.

`index.html` -> nuestro archivo maestro

## Licencia 

Licencia de producción de pares: [https://endefensadelsl.org/ppl_deed_es.html](https://endefensadelsl.org/ppl_deed_es.html)

[ ] Pendiente actualizar a versión reciente de revealjs

## Versión en vivo

Versión Onion:

[http://dwtxl2f7x64kp4zhcrhcebixtwb25s446pvmxgnqdjrmzeelgwyha5yd.onion/tails/index.html](http://dwtxl2f7x64kp4zhcrhcebixtwb25s446pvmxgnqdjrmzeelgwyha5yd.onion/tails/index.html)

Versión web:

[https://cacu.tech/tails/](https://cacu.tech/tails/)
